<?php
require_once("Animal.php");
require_once("Frog.php");
require_once("Ape.php");

$hewan = new animal("kucing");
$ape = new ape("Kera Sakti");
$frog = new frog("Buduk");

echo 'Name : '.$hewan->name. "<br>";
echo 'Legs : '.$hewan->legs."<br>";
echo 'Cold blooded : '.$hewan->cold_blooded."<br>";

echo "============================================= <br>";

echo 'Name : '.$ape->name. "<br>";
echo 'Legs : '.$ape->legs."<br>";
echo 'Cold blooded : '.$ape->cold_blooded."<br>";
echo 'Yell : '.$ape->yell()."<br>";
echo "============================================= <br>";

echo 'Name : '.$frog->name. "<br>";
echo 'Legs : '.$frog->legs."<br>";
echo 'Cold blooded : '.$frog->cold_blooded."<br>";
echo 'Jump : '.$frog->jump()."<br>";
?>